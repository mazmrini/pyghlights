import unittest

from pyghlights.lib.helpers.match import Match, PlayerStat


class ClipEventTests(unittest.TestCase):
    def setUp(self) -> None:
        self.match = Match.model_validate({"home_team": "home", "away_team": "away", "game_date": "2023-08-10"})

    def test__when_no_goals__top_scorers__returns_empty_list(self) -> None:
        result = self.match.top_scorers()

        self.assertEqual([], result)

    def test__when_goals__top_scorers__returns_best_scorers(self) -> None:
        self.match = self.match.home_scores("van")
        self.match = self.match.home_scores("zine")
        self.match = self.match.away_scores()
        self.match = self.match.home_scores("zine")
        self.match = self.match.home_scores("zine")
        self.match = self.match.away_scores()
        self.match = self.match.away_scores()
        self.match = self.match.home_scores("van")
        self.match = self.match.home_scores("giroud")
        expected = [
            PlayerStat(name="zine", goals=3),
            PlayerStat(name="van", goals=2),
            PlayerStat(name="giroud", goals=1),
        ]

        result = self.match.top_scorers()

        self.assertEqual(expected, result)

    def test__when_goals__nb_goals__returns_the_expected_values(self) -> None:
        self.match = self.match.home_scores("van")
        self.match = self.match.home_scores("zine")
        self.match = self.match.away_scores()
        self.match = self.match.home_scores("zine")
        self.match = self.match.home_scores("zine")

        self.assertEqual(3, self.match.nb_goals_for("zine"))
        self.assertEqual(1, self.match.nb_goals_for("van"))
        self.assertEqual(0, self.match.nb_goals_for("none"))

    def test__example_is_deserializable(self) -> None:
        Match.model_validate(Match.example())
