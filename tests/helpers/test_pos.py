import unittest
from typing import Tuple, Annotated

from parameterized import parameterized
from pydantic import BaseModel

from pyghlights.lib.helpers.pos import Pos, PosAsStr


class TestModel(BaseModel):
    pos: Annotated[Pos, PosAsStr]


class PosTests(unittest.TestCase):
    @parameterized.expand(  # type: ignore
        [
            ("normal", "2,1", (2, 1)),
            ("full digits", "333, 12", (333, 12)),
            ("zero", "0,0", (0, 0)),
        ]
    )
    def test__parse(self, _: str, value: str, expected: Tuple[int, int]) -> None:
        expected_pos = Pos(x=expected[0], y=expected[1])

        pos = Pos.parse(value)

        self.assertEqual(expected_pos, pos)

    def test__parse_fails_when_value_is_not_a_str(self) -> None:
        with self.assertRaises(AssertionError) as ctx:
            Pos.parse([1, 2, 3])

        self.assertEqual("must be a str", str(ctx.exception))

    def test__parse_fails_when_value_is_invalid_format(self) -> None:
        with self.assertRaises(AssertionError) as ctx:
            Pos.parse("hello")

        self.assertIn("must match regex", str(ctx.exception))

    def test__valid_pos_in_other_object(self) -> None:
        expected_pos = Pos(x=10, y=22)

        pos = TestModel.model_validate({"pos": "10, 22"}).pos

        self.assertEqual(pos, expected_pos)
