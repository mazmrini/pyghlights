import unittest

from parameterized import parameterized

from pyghlights.lib.helpers.team_name import TeamName


class TeamNameTest(unittest.TestCase):
    @parameterized.expand(
        [  # type: ignore
            ("short_name takes first 3", "abcd", "ABC"),
            ("two letters no space takes all", "ab", "AB"),
            ("two letters with space takes acronym", "a b", "AB"),
            ("one word but starts with FC", "FCbob", "FCB"),
            ("with FC", "FC Albatros Mous Aigris", "FCAMA"),
            ("with CS", "cs gentil club", "CSGC"),
        ]
    )
    def test__short_name_outputs_expected(self, _: str, name: str, expected: str) -> None:
        team_name = TeamName(name=name)

        self.assertEqual(expected, team_name.short())
