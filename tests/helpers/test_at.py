import unittest
from typing import Any

from parameterized import parameterized
from pydantic import ValidationError

from pyghlights.lib.helpers.at import At


class AtTests(unittest.TestCase):
    @parameterized.expand(  # type: ignore
        [
            ("normal", "19m03", "19:03"),
            ("with one digit", "1m01", "01:01"),
            ("full digits", "22m31", "22:31"),
            ("zero", "0m00", "00:00"),
            ("from int", 50, "00:50"),
            ("big int", 197, "03:17"),
            ("float lower", 49.44, "00:49"),
            ("float upper ", 197.99, "03:18"),
        ]
    )
    def test__parse_to_yt_format(self, _: str, value: Any, expected: str) -> None:
        at = At.parse(value)

        result = at.to_yt_format()

        self.assertEqual(expected, result)

    @parameterized.expand(  # type: ignore
        [
            ("list", [1]),
            ("invalid string", "hi"),
            ("negative int", -1),
            ("negative float", -1.5),
        ]
    )
    def test__parse_fails(self, _: str, value: Any) -> None:
        with self.assertRaises(ValidationError):
            At.model_validate({"seconds": value})

    @parameterized.expand(
        [  # type: ignore
            ("valid time", "1m20", 80),
            ("another valid", "51m55", 3115),
            ("as int", 210, 210),
            ("as float", 315.15, 315.15),
        ]
    )
    def test__to_seconds(self, _: str, value: Any, expected: float) -> None:
        at = At.parse(value)

        self.assertEqual(expected, at.seconds)
