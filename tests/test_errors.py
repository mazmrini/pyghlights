import unittest

from pyghlights.lib.errors import Errors


class ErrorTests(unittest.TestCase):
    def setUp(self) -> None:
        self.errors = Errors("tests")

    def test__when_empty_check_does_nothing(self) -> None:
        Errors("tests").check()

    def test__when_errors__check__raises_exception(self) -> None:
        self.errors.add("one")
        self.errors.add("two")
        self.errors.add("three")
        expected = "Errors in tests:\n" "- one\n" "- two\n" "- three\n"

        with self.assertRaises(Exception) as ctx:
            self.errors.check()

        self.assertEqual(expected, str(ctx.exception))
