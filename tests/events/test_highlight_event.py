import unittest
import yaml
from pydantic import ValidationError

from pyghlights.lib.events.events import HighlightEvent
from pyghlights.lib.helpers.at import At
from tests.utils.pydantic import PydanticErrorUtils


class HighlightEventTests(unittest.TestCase):
    def test__valid_init(self) -> None:
        dto = """
        description: highlight name
        at: 1m02
        buildup: "slow"
        """
        expected = HighlightEvent(at=At.parse("1m02"), description="highlight name", buildup="slow", skip_clip=False)

        event = HighlightEvent.model_validate(yaml.safe_load(dto))

        self.assertEqual(expected, event)

    def test__init_fails_when_invalid_data(self) -> None:
        dto = """
        description: ''
        at: 1m
        extra: field
        """
        expected = [
            PydanticErrorUtils.extra("extra"),
            PydanticErrorUtils.at("at"),
            PydanticErrorUtils.non_empty_string("description"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            HighlightEvent.model_validate(yaml.safe_load(dto))

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
