import unittest
import yaml
from pydantic import ValidationError

from pyghlights.lib.events.events import GoalEvent
from pyghlights.lib.helpers.at import At
from tests.utils.pydantic import PydanticErrorUtils


class GoalEventTests(unittest.TestCase):
    def test__valid_init_without_skip_clip_defaults_to_false(self) -> None:
        dto = """
        scorer: melap
        at: 8m08
        buildup: slow
        type: goal
        """
        expected = GoalEvent(at=At.parse("8m08"), scorer="melap", buildup="slow", skip_clip=False)

        event = GoalEvent.model_validate(yaml.safe_load(dto))

        self.assertEqual(expected, event)

    def test__valid_init_with_skip_clip(self) -> None:
        dto = """
        scorer: melap
        at: 8m08
        buildup: slow
        type: goal
        skip_clip: true
        """
        expected = GoalEvent(at=At.parse("8m08"), scorer="melap", buildup="slow", skip_clip=True)

        event = GoalEvent.model_validate(yaml.safe_load(dto))

        self.assertEqual(expected, event)

    def test__init_fails_when_invalid_data(self) -> None:
        dto = """
        at: 8m08
        extra: field
        buildup: fast
        type: goal
        scorer: ''
        """
        expected = [PydanticErrorUtils.non_empty_string("scorer"), PydanticErrorUtils.extra("extra")]

        with self.assertRaises(ValidationError) as ctx:
            GoalEvent.model_validate(yaml.safe_load(dto))

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
