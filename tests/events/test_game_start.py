import unittest
import yaml
from pydantic import ValidationError

from pyghlights.lib.events.events import GameStartEvent

from pyghlights.lib.helpers.at import At
from tests.utils.pydantic import PydanticErrorUtils


class GameStartEventTests(unittest.TestCase):
    def test__valid_init(self) -> None:
        dto = "at: 0m14\ntype: game_start"
        expected = GameStartEvent(at=At.parse("0m14"), skip_clip=False)

        event = GameStartEvent.model_validate(yaml.safe_load(dto))

        self.assertEqual(expected, event)

    def test__init_fails_when_invalid_data(self) -> None:
        dto = "other: field"
        expected = [PydanticErrorUtils.extra("other"), PydanticErrorUtils.required("at")]

        with self.assertRaises(ValidationError) as ctx:
            GameStartEvent.model_validate(yaml.safe_load(dto))

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
