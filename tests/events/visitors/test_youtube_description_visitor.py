import unittest

from pyghlights.lib.events.visitors.youtube_description_visitor import YoutubeDescriptionVisitor
from tests.utils import events


class YoutubeDescriptionVisitorTests(unittest.TestCase):
    def setUp(self) -> None:
        self.match = events.first_game_match()
        self.visitor = YoutubeDescriptionVisitor(self.match)

    def test__visit_from_first_game_events(self) -> None:
        expected = events.expected_first_game_youtube_desc()

        description = self.visitor.visit(self.match.events())

        self.assertEqual(expected, description)
