import unittest
from pathlib import Path

from pydantic import ValidationError

from pyghlights.lib.events.visitors.clips.config.clip_config import ClipConfig
from tests.utils.pydantic import PydanticErrorUtils


class ClipConfigTests(unittest.TestCase):
    def test__init_with_existing_font_works(self) -> None:
        ClipConfig.from_file(Path("tests/test_files/clip_config/valid.yml"))

    def test__init_with_invalid_configs(self) -> None:
        expected = [
            PydanticErrorUtils.extra("extra"),
            # goal
            PydanticErrorUtils.positive_int("goal", "banner_duration"),
            PydanticErrorUtils.custom("color must exist", "goal", "font", "color"),
            PydanticErrorUtils.positive_int("goal", "buildup", "fast", "end_after"),
            PydanticErrorUtils.positive_int("goal", "buildup", "slow", "start_before"),
            # intro
            PydanticErrorUtils.extra("intro", "extra"),
            PydanticErrorUtils.path_exists("intro", "bg_path"),
            PydanticErrorUtils.custom("font must exist", "intro", "font", "family"),
            PydanticErrorUtils.positive_int("intro", "font", "size"),
            # outro
            PydanticErrorUtils.color_int("outro", "default_bg_color", 1),
            # score_overlay
            PydanticErrorUtils.extra("score_overlay", "extra"),
            PydanticErrorUtils.path_exists("score_overlay", "bg_path"),
            PydanticErrorUtils.extra("score_overlay", "score_font", "extra"),
            PydanticErrorUtils.color_int("score_overlay", "team_bg_color", 0),
            PydanticErrorUtils.positive_int("score_overlay", "team_font", "size"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            ClipConfig.from_file(Path("tests/test_files/clip_config/invalid.yml"))

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(actual, expected)
