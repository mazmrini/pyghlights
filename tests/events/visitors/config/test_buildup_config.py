import unittest


from pyghlights.lib.events.visitors.clips.config.buildup import BuildupConfig, BuildupTimeRange


class BuildupConfigTests(unittest.TestCase):
    def setUp(self) -> None:
        self.buildup_config = BuildupConfig(
            very_fast=BuildupTimeRange.new(5, 4),
            fast=BuildupTimeRange.new(7, 4),
            normal=BuildupTimeRange.new(9.5, 4),
            slow=BuildupTimeRange.new(12, 4),
            very_slow=BuildupTimeRange.new(15, 4),
        )

    def test__get_existing_config_returns_expected_range(self) -> None:
        result = self.buildup_config.get_or_normal("very_fast")

        self.assertEqual(self.buildup_config.very_fast, result)

    def test__get_unknown_config__returns_normal_range(self) -> None:
        result = self.buildup_config.get_or_normal("unknown")

        self.assertEqual(self.buildup_config.normal, result)
