import unittest
import yaml
from pydantic import ValidationError

from pyghlights.lib.events.events import ClipEvent
from pyghlights.lib.helpers.at import At
from tests.utils.pydantic import PydanticErrorUtils


class ClipEventTests(unittest.TestCase):
    def test__valid_init(self) -> None:
        dto = """
        description: clip name
        at: 1m02
        start_before: 1
        end_after: 10
        """
        expected = ClipEvent(
            at=At.parse("1m02"), description="clip name", start_before=1, end_after=10, skip_clip=False
        )

        event = ClipEvent.model_validate(yaml.safe_load(dto))

        self.assertEqual(expected, event)

    def test__init_fails_when_invalid_data(self) -> None:
        dto = """
        at: 15m
        description: ''
        start_before: -1
        end_after: -1
        extra: field
        """
        expected = [
            PydanticErrorUtils.non_empty_string("description"),
            PydanticErrorUtils.extra("extra"),
            PydanticErrorUtils.at("at"),
            PydanticErrorUtils.non_negative_number("start_before"),
            PydanticErrorUtils.non_negative_number("end_after"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            ClipEvent.model_validate(yaml.safe_load(dto))

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
