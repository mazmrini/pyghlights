from typing import Dict, Union, Any, List

from pydantic import ValidationError

Loc = Union[str, int]


class PydanticErrorUtils:
    @staticmethod
    def required(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "Field required"}

    @staticmethod
    def non_empty_string(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "String should have at least 1 characters"}

    @staticmethod
    def extra(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "Extra inputs are not permitted"}

    @staticmethod
    def at(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "Assertion failed, timestamp has an invalid value"}

    @staticmethod
    def path_exists(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "Assertion failed, path must exist"}

    @staticmethod
    def color_int(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "Assertion failed, value must be between 0 and 255"}

    @staticmethod
    def positive_int(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "Input should be greater than 0"}

    @staticmethod
    def non_negative_number(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "Input should be greater than or equal to 0"}

    @staticmethod
    def custom(msg: str, *loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": f"Assertion failed, {msg}"}

    @staticmethod
    def from_exception(exc: ValidationError) -> List[Dict[str, Any]]:
        return [{"loc": e["loc"], "msg": e["msg"]} for e in exc.errors()]
