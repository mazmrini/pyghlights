from datetime import date
from pyghlights.lib.events.events import (
    ClipEvent,
    GoalEvent,
    GameStartEvent,
    GameEndEvent,
    HighlightEvent,
)
from pyghlights.lib.helpers.at import At
from pyghlights.lib.helpers.const import AWAY
from pyghlights.lib.helpers.match import Match
from pyghlights.lib.helpers.team_name import TeamName


def first_game_match() -> Match:
    return Match(
        home_team=TeamName(name="FC Jambes Molles"),
        away_team=TeamName(name="FC Chatons"),
        game_date=date(2023, 9, 26),
        game_events=[
            ClipEvent(at=At.parse(30), end_after=15, description="Coach speech"),
            GameStartEvent(at=At.parse(45)),
            HighlightEvent(at=At.parse(50), description="Van chance", buildup="fast"),
            GoalEvent(at=At.parse("1m06"), scorer="zine", buildup="normal"),
            GoalEvent(at=At.parse("1m18"), scorer=AWAY, buildup="fast"),
            GoalEvent(at=At.parse("1m37"), scorer=AWAY, buildup="normal"),
            GoalEvent(at=At.parse("1m52"), scorer="lea", buildup="slow"),
            GoalEvent(at=At.parse("2m07"), scorer="zine", buildup="normal"),
            HighlightEvent(at=At.parse("2m26"), description="zine chance", buildup="slow"),
            ClipEvent(at=At.parse("2m47"), description="Mi-temps", end_after=3),
            HighlightEvent(at=At.parse(187), description="cath chance", buildup="normal"),
            GoalEvent(at=At.parse("3m18"), scorer=AWAY, buildup="normal"),
            HighlightEvent(at=At.parse("3m29"), description="nice play", buildup="normal"),
            HighlightEvent(at=At.parse("3m43"), description="melaq shot", buildup="normal"),
            HighlightEvent(at=At.parse("3m47"), description="rex hit", buildup="normal"),
            HighlightEvent(at=At.parse("3m57"), description="van header", buildup="normal"),
            GoalEvent(at=At.parse("4m04"), scorer="van", buildup="normal"),
            HighlightEvent(at=At.parse("4m16"), description="lea shot", buildup="slow"),
            GoalEvent(at=At.parse("4m21"), scorer=AWAY, buildup="fast"),
            HighlightEvent(at=At.parse("4m30"), description="lea shot", buildup="normal"),
            GoalEvent(at=At.parse("4m37"), scorer="gadras", buildup="normal"),
            HighlightEvent(at=At.parse("4m52"), description="ch1lo shake and bake", buildup="normal"),
            GoalEvent(at=At.parse("5m01"), scorer="giroud", buildup="fast"),
            GoalEvent(at=At.parse("5m12"), scorer="zine", buildup="slow"),
            GoalEvent(at=At.parse("5m40"), scorer="zine", buildup="slow"),
            GameEndEvent(at=At.parse("5m55")),
        ],
    )


def expected_first_game_youtube_desc() -> str:
    return "\n".join(
        (
            "Match du 2023-09-26",
            "FC Jambes Molles v FC Chatons\n",
            "00:30 Coach speech",
            "00:45 Début du match",
            "00:50 Van chance",
            "01:06 ⚽ But de zine (1-0)",
            "01:18 ⚽ But de FC Chatons (1-1)",
            "01:37 ⚽ But de FC Chatons (1-2)",
            "01:52 ⚽ But de lea (2-2)",
            "02:07 ⚽ But de zine (3-2)",
            "02:26 zine chance",
            "02:47 Mi-temps",
            "03:07 cath chance",
            "03:18 ⚽ But de FC Chatons (3-3)",
            "03:29 nice play",
            "03:43 melaq shot",
            "03:47 rex hit",
            "03:57 van header",
            "04:04 ⚽ But de van (4-3)",
            "04:16 lea shot",
            "04:21 ⚽ But de FC Chatons (4-4)",
            "04:30 lea shot",
            "04:37 ⚽ But de gadras (5-4)",
            "04:52 ch1lo shake and bake",
            "05:01 ⚽ But de giroud (6-4)",
            "05:12 ⚽ But de zine (7-4)",
            "05:40 ⚽ But de zine (8-4)",
            "05:55 Fin du match\n",
        )
    )
