import unittest
from pathlib import Path

from parameterized import parameterized

from pyghlights.lib.game_config import GameConfig
from pyghlights.lib.events.events import (
    HighlightEvent,
    GameStartEvent,
    GoalEvent,
    ClipEvent,
    GameEndEvent,
)
from pyghlights.lib.events.visitors.youtube_description_visitor import YoutubeDescriptionVisitor
from pyghlights.lib.helpers.match import Match
from tests.utils import events


class ConfigTests(unittest.TestCase):
    def test__when_valid_config_in_file__from_config_file__out_uses_parent_directory(self) -> None:
        config = GameConfig.from_config_file(Path("tests/test_files/config/valid.yml"))

        self.assertIsInstance(config.match.game_events[0], GameStartEvent)
        self.assertIsInstance(config.match.game_events[1], HighlightEvent)
        self.assertIsInstance(config.match.game_events[2], GoalEvent)
        self.assertIsInstance(config.match.game_events[3], ClipEvent)
        self.assertIsInstance(config.match.game_events[4], GameEndEvent)

        self.assertEqual(
            [Path("tests/test_files/config/a.mp4"), Path("tests/test_files/config/b.mp4")], config.clip_paths
        )
        self.assertEqual(Path("tests/test_files/config/out"), config.out_dir)
        self.assertEqual(Path("tests/test_files/config/out/highlights.mp4"), config.out_clip_path)
        self.assertEqual(Path("tests/test_files/config/out/youtube.txt"), config.out_youtube_path)

    @parameterized.expand(  # type: ignore
        [
            ("invalid paths", ["tests/test_files/unknown"], "tests/test_files/unknown/file.mp4"),
            ("invalid files path", ["tests/test_files/config"], "tests/test_files/unknown_dir"),
            ("out path exists", ["tests/test_files/config"], "tests/test_files/config/a.mp4"),
        ]
    )
    def test__when_invalid_config__init__raises_error(self, _: str, clips: str, out_dir: str) -> None:
        with self.assertRaises(Exception) as ctx:
            GameConfig(
                self.__empty_match(),
                [Path(c) for c in clips],
                Path(out_dir),
                Path("intro"),
                Path("outro"),
                Path("score"),
            )

        msg = str(ctx.exception)

        self.assertIn("Clip", msg)
        self.assertIn("Config directory", msg)

    def test_when_invalid_config__from_config_file__raises_error(self) -> None:
        with self.assertRaises(Exception) as ctx:
            GameConfig.from_config_file(Path("tests/test_files/config/invalid.yml"))

        msg = str(ctx.exception)

        self.assertIn("validation errors", msg)

    def test_when_file_does_not_exist__from_config__file_raises_error(self) -> None:
        with self.assertRaises(Exception) as ctx:
            GameConfig.from_config_file(Path("tests/test_files/config/unknown.yml"))

        msg = str(ctx.exception)

        self.assertIn("No such file", msg)

    def test__valid_intro_and_outro_files__from_config__returns_paths(self) -> None:
        config = GameConfig.from_config_file(Path("tests/test_files/config/valid.yml"))

        self.assertIsNotNone(config.intro_bg_path)
        self.assertEqual(Path("tests/test_files/config/intro.png"), config.intro_bg_path)
        self.assertIsNotNone(config.outro_bg_path)
        self.assertEqual(Path("tests/test_files/config/outro.png"), config.outro_bg_path)

    def test__invalid_bg_files__from_config__returns_None(self) -> None:
        valid_config = GameConfig.from_config_file(Path("tests/test_files/config/valid.yml"))
        config = GameConfig(
            valid_config.match,
            valid_config.clip_paths,
            valid_config.config_dir,
            Path("invalid_intro.png"),
            Path("invalid_outro.png"),
            Path("invalid_score.png"),
        )

        self.assertIsNone(config.intro_bg_path)
        self.assertIsNone(config.outro_bg_path)
        self.assertIsNone(config.score_overlay_bg_path)

    def test__integration_from_yaml_configuration_to_youtube_description(self) -> None:
        expected = events.expected_first_game_youtube_desc()
        config = GameConfig.from_config_file(Path("tests/test_files/config/first_game.yml"))
        youtube = YoutubeDescriptionVisitor(config.match)

        description = youtube.visit(config.match.events())

        self.assertEqual(expected, description)

    def __empty_match(self) -> Match:
        return Match.model_validate({"home_team": "home", "away_team": "away", "game_date": "2023-01-01"})
