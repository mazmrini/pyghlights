# pyghlights


Automatically create highlights from timestamps. Primarly designed for sports highlights.


## Requirements

- [sbin](https://gitlab.com/mazmrini/bin)
- Download [ImageMagick](https://imagemagick.org/script/download.php) and set the `IMAGEMAGICK_BINARY`
  environment variable to point to the executable file

Its recommended to install `sbin` globally.
```shell
pip install sbin
```

## How to run the program
### Local setup
```bash
bin up
```

### Required file structure
`pyghlights` requires a folder structure in order for it to generate your clip.

It needs a folder that contains the following:
- `*.mp4`: Clips to go through. Needs at least 1 clip.
- `some_events.yml`: An event file with an arbitrary name (see `events.example.yml`)
- `intro.png`: (optional) A background for the introduction clip
- `outro.png`: (optional) A background for the outro clip
- `score_overlay.png`: (optional) Using this requires tweaks to your `pyghlights.yml` configuration file  

A full valid structure looks like:
```
my_game/
  clip_1.mp4
  clip_2.mp4
  clip_3.mp4
  game.yml
  intro.png
  outro.png
  score_overlay.png
```

### Output
`pyghlights` output 2 files in your game folder under an `out` directory:

1. `highlights.mp4`: Contains the rendered video
2. `youtube.txt`: Contains a valid youtube description with event timestamps

It will look something like this:
```
my_game/
  ...
  out/
    highlights.mp4
    youtube.txt
```

### (optional) `pyghlights` settings
At the repository root, you can have a `pyghlights.yml` file which contains settings the software
will use. `pyghlights.example.yml` contains the default settings.

Feel free to copy it as `pyghlights.yml` at the repo root and tweak the settings to your liking.

Note that  

### Run `pyghlights`
There are 2 ways to run the program:
```bash
# through CLI
bin clip <path to event.yml file>

# will pop a window to select the event file
bin ui
```

## Local development
### Useful dev commands

```
bin req
bin test
bin up
bin verify
bin lint/format/typecheck

# pyghlights specific
bin generate config  # generate example configuration files
```
