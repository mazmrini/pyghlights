"""
usage: [--version] [--help] <config_file>

Mandatory args:
  config_file    Path to the yaml game configuration file

options:
  -v, --version
  -h, --help

"""
from pathlib import Path
from typing import Tuple

import yaml
from docopt import docopt
import traceback
from moviepy.video.VideoClip import TextClip

from pyghlights.lib.clip_crew import ClipCrew
from pyghlights.lib.game_config import GameConfig
from pyghlights.lib.events.visitors.clips.config.clip_config import ClipConfig
from pyghlights.lib.helpers.match import Match
from pyghlights.lib.logger import Logger


def __pyghlights_config_file() -> Path:
    return Path(__file__).parent.parent / "pyghlights.yml"


def __load_configs(game_config_path: Path) -> Tuple[GameConfig, ClipConfig]:
    game_config = GameConfig.from_config_file(game_config_path)

    clip_config = ClipConfig.from_file(__pyghlights_config_file())
    clip_config.intro.bg_path = game_config.intro_bg_path
    clip_config.outro.bg_path = game_config.outro_bg_path
    clip_config.score_overlay.bg_path = game_config.score_overlay_bg_path

    return game_config, clip_config


def __clip_it(game_config_path: Path) -> None:
    try:
        game_config, clip_config = __load_configs(game_config_path)

        clip_crew = ClipCrew(game_config.clip_paths, clip_config)
        with clip_crew.director() as director:
            clip, clip_events = director.make_clip(game_config.match)
            clip.write(game_config.out_clip_path)

        youtube_description = clip_crew.make_youtube_description(game_config.match, clip_events)
        Logger.log(f"Writing youtube description @ {game_config.out_youtube_path}")
        game_config.out_youtube_path.write_text(youtube_description, encoding="utf-8")
    except Exception as e:
        print(f"\n{e}")
        traceback.print_exc()
        raise Exception()


def cli() -> None:
    docopt_args = docopt(__doc__, version="0.0.0")

    config_file = docopt_args["<config_file>"]
    __clip_it(Path(config_file))


def ui() -> None:
    import tkinter as tk
    from tkinter import filedialog as fd

    root = tk.Tk()
    root.withdraw()

    event_file = fd.askopenfilename(title="Sélectionner un fichier d'évènements", filetypes=[("YAML files", "*.yml")])
    if event_file:
        __clip_it(Path(event_file))


def generate_config() -> None:
    root_dir = Path(__file__).parent.parent
    clip_config = ClipConfig().model_dump(mode="json")
    clip_config_content = (
        yaml.dump(clip_config)
        .replace(":\n  - 33", ": [33, 33, 33]")
        .replace("  - 33\n", "")
        .replace(":\n  - 222", ": [222, 222, 222]")
        .replace("  - 222\n", "")
    )
    (root_dir / "pyghlights.example.yml").write_text(clip_config_content)

    match = Match.example()
    (root_dir / "events.example.yml").write_text(yaml.dump(match))


def colors() -> None:
    print("\n".join(c.decode() for c in TextClip.list("color")))


def fonts() -> None:
    print("\n".join(f.decode() for f in TextClip.list("font")))


if __name__ == "__main__":
    cli()
