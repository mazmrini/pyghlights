from __future__ import annotations

from pathlib import Path
from typing import List, Optional

from moviepy.editor import concatenate_videoclips, VideoClip

from pyghlights.lib.clips.fxs import VideoFx
from pyghlights.lib.helpers.at import At
from pyghlights.lib.logger import Logger


class Clip:
    def __init__(self, fx: VideoFx, clips: Optional[List[VideoClip]] = None) -> None:
        self._video_fx = fx
        self._clips = clips or []

    def add(self, clip: VideoClip) -> Clip:
        self._clips.append(clip)

        return self

    def merge(self) -> VideoClip:
        return concatenate_videoclips(self.__with_fx())

    def concatenate(self) -> VideoClip:
        return concatenate_videoclips(self.__with_fx())

    def write(self, path: Path) -> None:
        out_clip = self.merge()
        Logger.log(f"Writing file of: {At(seconds=out_clip.duration).to_yt_format()}")
        path.parent.mkdir(exist_ok=True)
        out_clip.write_videofile(str(path), threads=16, bitrate="12000k")

    def __with_fx(self) -> List[VideoClip]:
        return [self._video_fx(clip) for clip in self._clips]
