from __future__ import annotations
from typing import Union, Literal, Callable

from moviepy.video.VideoClip import VideoClip
from moviepy.video.compositing import transitions as transfx


VideoFx = Callable[[VideoClip], VideoClip]
VideoFXValue = Union[Literal["none"], Literal["crossfade"]]


class VideoFxs:
    @staticmethod
    def apply(value: VideoFXValue, clip: VideoClip) -> VideoClip:
        return VideoFxs.from_str(value)(clip)

    @staticmethod
    def from_str(value: VideoFXValue) -> VideoFx:
        if value == "none":
            return VideoFxs.no_fx

        if value == "crossfade":
            return VideoFxs.crossfade_in_out

        raise Exception(f"Invalid vfx value '{value}'")

    @staticmethod
    def crossfade_in_out(clip: VideoClip) -> VideoClip:
        return clip.fx(transfx.crossfadein, 0.33).fx(transfx.crossfadeout, 0.33)

    @staticmethod
    def no_fx(clip: VideoClip) -> VideoClip:
        return clip
