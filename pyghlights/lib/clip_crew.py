from __future__ import annotations
from pathlib import Path
from typing import List, Tuple, Any

from moviepy.editor import VideoFileClip, concatenate_videoclips, VideoClip

from pyghlights.lib.clips.clip import Clip
from pyghlights.lib.clips.fxs import VideoFxs
from pyghlights.lib.events.events import (
    GameEvent,
    CompositeEvent,
    OutroEvent,
    IntroEvent,
)
from pyghlights.lib.events.visitors.clips.add_score_overlay_visitor import AddScoreOverlayVisitor
from pyghlights.lib.events.visitors.clips.snip_highlights_visitor import SnipHighlightsVisitor
from pyghlights.lib.events.visitors.clips.add_clip_overlay_visitor import AddClipOverlayVisitor
from pyghlights.lib.events.visitors.clips.config.clip_config import ClipConfig
from pyghlights.lib.events.visitors.clips.clip_visitor import ClipVisitorFactory
from pyghlights.lib.events.visitors.youtube_description_visitor import YoutubeDescriptionVisitor
from pyghlights.lib.helpers.match import Match


class _ClipDirector:
    def __init__(self, clip_paths: List[Path], clip_config: ClipConfig) -> None:
        self.__clip_paths = clip_paths
        self.__config = clip_config

        self.__clips: List[VideoClip] = []
        self.__full_clip = VideoClip()

    def __enter__(self) -> _ClipDirector:
        self.__clips = [VideoFileClip(str(p)) for p in self.__clip_paths]
        self.__full_clip = concatenate_videoclips(self.__clips)

        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any) -> None:
        self.__full_clip.close()
        for c in self.__clips:
            c.close()

    def make_clip(self, match: Match) -> Tuple[Clip, GameEvent]:
        event: GameEvent = CompositeEvent.new([IntroEvent.new(), *match.events(), OutroEvent.new()])
        visitors: List[ClipVisitorFactory] = [AddScoreOverlayVisitor, SnipHighlightsVisitor, AddClipOverlayVisitor]
        out_clip = self.__full_clip
        for visitor in visitors:
            out_clip, event = event.visit(visitor(out_clip, self.__config, match))

        return Clip(VideoFxs.no_fx, [out_clip]), event


class ClipCrew:
    def __init__(self, clip_paths: List[Path], clip_config: ClipConfig) -> None:
        self.__clip_paths = clip_paths
        self.__config = clip_config

    def director(self) -> _ClipDirector:
        return _ClipDirector(self.__clip_paths, self.__config)

    def make_youtube_description(self, match: Match, event: GameEvent) -> str:
        return event.visit(YoutubeDescriptionVisitor(match))
