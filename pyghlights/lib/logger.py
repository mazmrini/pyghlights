class Logger:
    @staticmethod
    def log(msg: str) -> None:
        for m in msg.split("\n"):
            print(f"Pyghlights - {m}")
