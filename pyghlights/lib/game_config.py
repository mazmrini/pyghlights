from __future__ import annotations

from pathlib import Path
from typing import List, Optional

import yaml

from pyghlights.lib.errors import Errors
from pyghlights.lib.helpers.match import Match
from pyghlights.lib.logger import Logger


class GameConfig:
    def __init__(
        self,
        match: Match,
        clip_paths: List[Path],
        config_dir: Path,
        intro_bg_path: Path,
        outro_bg_path: Path,
        score_overlay_bg_path: Path,
    ) -> None:
        errors = Errors("config")
        for clip_path in clip_paths:
            if not clip_path.is_file():
                errors.add(f"Clip [{clip_path}] is not a valid file path")

        if config_dir.is_file() or not config_dir.exists():
            errors.add(f"Config directory [{config_dir}] is invalid")

        errors.check()
        self.match = match
        self.clip_paths = clip_paths
        self.config_dir = config_dir
        self.out_dir = config_dir / "out"
        self.__intro_bg_path = intro_bg_path
        self.__outro_bg_path = outro_bg_path
        self.__score_overlay_bg_path = score_overlay_bg_path

    @property
    def out_clip_path(self) -> Path:
        return self.out_dir / "highlights.mp4"

    @property
    def out_youtube_path(self) -> Path:
        return self.out_dir / "youtube.txt"

    @property
    def intro_bg_path(self) -> Optional[Path]:
        if self.__intro_bg_path.exists():
            return self.__intro_bg_path

        return None

    @property
    def outro_bg_path(self) -> Optional[Path]:
        if self.__outro_bg_path.exists():
            return self.__outro_bg_path

        return None

    @property
    def score_overlay_bg_path(self) -> Optional[Path]:
        if self.__score_overlay_bg_path.exists():
            return self.__score_overlay_bg_path

        return None

    @classmethod
    def from_config_file(cls, config_path: Path) -> GameConfig:
        Logger.log(f"Using game event file: {config_path}")
        content = yaml.safe_load(config_path.read_bytes())
        match = Match.model_validate(content)

        Logger.log(f"Looking for mp4 clips in {config_path.parent}...")
        clip_paths = sorted(config_path.parent.glob("*.mp4"))
        Logger.log(f"{len(clip_paths)} clip(s) found: " + ", ".join([c.name for c in clip_paths]))

        return GameConfig(
            match,
            clip_paths,
            config_path.parent,
            config_path.parent / "intro.png",
            config_path.parent / "outro.png",
            config_path.parent / "score_overlay.png",
        )
