from typing import List


class Errors:
    def __init__(self, category: str) -> None:
        self.category = category
        self.errors: List[str] = []

    def add(self, error: str) -> None:
        self.errors.append(error)

    def check(self) -> None:
        if len(self.errors) == 0:
            return

        formatted = "\n".join([f"- {err}" for err in self.errors])
        formatted = f"Errors in {self.category}:\n{formatted}\n"

        raise Exception(formatted)
