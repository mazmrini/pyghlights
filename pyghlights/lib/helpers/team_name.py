from __future__ import annotations
from typing import Any
from pydantic import BaseModel, ConfigDict


class TeamName(BaseModel):
    model_config = ConfigDict(str_min_length=1)

    name: str

    @classmethod
    def parse(cls, v: Any) -> TeamName:
        if isinstance(v, TeamName):
            return v

        return TeamName(name=v)

    def short(self) -> str:
        name = self.name.upper()
        if " " not in name:
            return name[:3]

        if len(name) < 4:
            return name.replace(" ", "")

        if name.startswith("FC") or name.startswith("CS"):
            return name[:2] + self.__acronym_of(name[2:])

        return self.__acronym_of(name)

    def __acronym_of(self, name: str) -> str:
        return "".join([word[0] for word in name.split()])
