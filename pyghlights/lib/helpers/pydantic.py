from __future__ import annotations
from pathlib import Path
from typing import Optional, Annotated

from pydantic import BeforeValidator


def validate_path_exists(value: Optional[str]) -> Optional[Path]:
    if value is None:
        return value

    assert Path(value).exists(), "path must exist"

    return Path(value)


def validate_color_int(value: int) -> int:
    assert 0 <= value <= 255, "value must be between 0 and 255"

    return value


ColorInt = Annotated[int, BeforeValidator(validate_color_int)]
OptionalPath = Annotated[Optional[Path], BeforeValidator(validate_path_exists)]
