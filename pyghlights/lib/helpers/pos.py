from __future__ import annotations

from typing import Any

from pydantic import BaseModel, NonNegativeInt, BeforeValidator, model_serializer
import re


POS_REGEX = r"^\d+, ?\d+$"


class Pos(BaseModel):
    x: NonNegativeInt
    y: NonNegativeInt

    @model_serializer
    def serialize_pos(self) -> str:
        return f"{self.x},{self.y}"

    @classmethod
    def parse(cls, v: Any) -> Pos:
        assert isinstance(v, str), "must be a str"
        assert re.match(POS_REGEX, v), f"must match regex '{POS_REGEX}'"

        x, y = v.split(",")

        return Pos(x=int(x), y=int(y.strip()))


PosAsStr = BeforeValidator(Pos.parse)
