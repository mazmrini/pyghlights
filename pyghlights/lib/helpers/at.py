from __future__ import annotations

import re
import sys
from typing import Tuple, Any

from pydantic import NonNegativeFloat, BaseModel, BeforeValidator

AT_REGEX = r"^\d\d?m\d\d$"


class At(BaseModel):
    seconds: NonNegativeFloat

    @classmethod
    def zero(cls) -> At:
        return At(seconds=0)

    @classmethod
    def infinity(cls) -> At:
        return At(seconds=sys.float_info.max)

    @classmethod
    def parse(cls, v: Any) -> At:
        if isinstance(v, At):
            return v

        if isinstance(v, int) or isinstance(v, float):
            return At(seconds=float(v))

        if not isinstance(v, str):
            pass
        assert isinstance(v, str), "timestamp has an invalid value"
        assert re.match(AT_REGEX, v), "timestamp has an invalid value"

        minutes, seconds = v.split("m")
        return At(seconds=float(minutes) * 60 + float(seconds))

    def to_yt_format(self) -> str:
        m, s = self.__to_minutes_seconds()
        return "{:02d}:{:02d}".format(m, s)

    def minus_seconds(self, secs: float) -> At:
        return At(seconds=self.seconds - secs)

    def plus_seconds(self, secs: float) -> At:
        return At(seconds=self.seconds + secs)

    def __to_minutes_seconds(self) -> Tuple[int, int]:
        rounded_seconds = round(self.seconds)
        minutes = int(rounded_seconds // 60)
        seconds = int(rounded_seconds % 60)

        return minutes, seconds


AtAsDto = BeforeValidator(At.parse)
