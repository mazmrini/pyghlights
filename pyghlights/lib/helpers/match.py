from __future__ import annotations

from datetime import date
from typing import Dict, List, Annotated, Any

from pydantic import BaseModel, ConfigDict, BeforeValidator, Extra

from pyghlights.lib.events.events import SerializableGameEvent, GameEvent
from pyghlights.lib.helpers.team_name import TeamName


class Score(BaseModel):
    home: int = 0
    away: int = 0

    def home_team_scored(self) -> Score:
        return Score(home=self.home + 1, away=self.away)

    def away_team_scored(self) -> Score:
        return Score(home=self.home, away=self.away + 1)


class PlayerStat(BaseModel):
    name: str
    goals: int = 0


TeamNameAsStr = BeforeValidator(lambda n: TeamName.parse(n))


class Match(BaseModel):
    model_config = ConfigDict(extra=Extra.forbid)

    home_team: Annotated[TeamName, TeamNameAsStr]
    away_team: Annotated[TeamName, TeamNameAsStr]
    game_date: date
    game_events: List[SerializableGameEvent] = []
    score: Score = Score()
    playerStats: Dict[str, int] = {}

    def events(self) -> List[GameEvent]:
        return self.game_events  # type: ignore

    def top_scorers(self) -> List[PlayerStat]:
        if len(self.playerStats) == 0:
            return []

        top = sorted(list(self.playerStats.items()), key=lambda e: e[1], reverse=True)
        return [PlayerStat(name=scorer, goals=goals) for scorer, goals in top]

    def nb_goals_for(self, scorer: str) -> int:
        return self.playerStats.get(scorer, 0)

    def home_scores(self, scorer: str) -> Match:
        m = self.model_copy()
        m.score = m.score.home_team_scored()
        m.playerStats[scorer] = m.nb_goals_for(scorer) + 1

        return m

    def away_scores(self) -> Match:
        m = self.model_copy()
        m.score = m.score.away_team_scored()

        return m

    def restart(self) -> Match:
        m = self.model_copy()
        m.score = Score()
        m.playerStats = {}

        return m

    @staticmethod
    def example() -> Dict[str, Any]:
        return {
            "home_team": "home",
            "away_team": "away",
            "game_date": "2023-10-22",
            "game_events": GameEvent.examples(),
        }
