from __future__ import annotations
from typing import List

from pyghlights.lib.events.events import (
    GameEvent,
    IntroEvent,
    ClipEvent,
    GoalEvent,
    OutroEvent,
    GameEventVisitor,
    GameStartEvent,
    GameEndEvent,
    HighlightEvent,
)
from pyghlights.lib.helpers.match import Match


class ScoreKeeperVisitor(GameEventVisitor[None]):
    def __init__(self, match: Match) -> None:
        self.is_game_started = False
        self.is_game_ended = False
        self.match = match

    def visit(self, events: List[GameEvent]) -> None:
        for e in events:
            e.visit(self)

    def visit_intro(self, intro: IntroEvent) -> None:
        pass

    def visit_game_start(self, game_start: GameStartEvent) -> None:
        self.match = self.match.restart()
        self.is_game_started = True
        self.is_game_ended = False

    def visit_game_end(self, game_end: GameEndEvent) -> None:
        self.is_game_ended = True

    def visit_clip(self, clip: ClipEvent) -> None:
        pass

    def visit_highlight(self, highlight: HighlightEvent) -> None:
        pass

    def visit_goal(self, goal: GoalEvent) -> None:
        if goal.is_away_goal():
            self.match = self.match.away_scores()
        else:
            self.match = self.match.home_scores(goal.scorer)

    def visit_outro(self, outro: OutroEvent) -> None:
        pass

    def restart(self) -> None:
        self.match = self.match.restart()
        self.is_game_started = False
        self.is_game_ended = False
