from typing import List

from pyghlights.lib.events.events import (
    GameEvent,
    IntroEvent,
    ClipEvent,
    GoalEvent,
    OutroEvent,
    GameEventVisitor,
    GameStartEvent,
    GameEndEvent,
    HighlightEvent,
)
from pyghlights.lib.events.visitors.score_keeper_visitor import ScoreKeeperVisitor
from pyghlights.lib.helpers.match import Match


class YoutubeDescriptionVisitor(GameEventVisitor[str]):
    def __init__(self, match: Match) -> None:
        self.__score_keeper = ScoreKeeperVisitor(match)
        self.__description = ""

    def visit(self, events: List[GameEvent]) -> str:
        match = self.__score_keeper.match
        description = f"Match du {match.game_date.isoformat()}\n"
        description += f"{match.home_team.name} v {match.away_team.name}\n\n"
        for event in events:
            event.visit(self.__score_keeper)
            description += event.visit(self)

        return description

    def visit_intro(self, intro: IntroEvent) -> str:
        return ""

    def visit_game_start(self, game_start: GameStartEvent) -> str:
        return self.__format_moment(game_start, "Début du match")

    def visit_game_end(self, game_end: GameEndEvent) -> str:
        return self.__format_moment(game_end, "Fin du match")

    def visit_clip(self, clip: ClipEvent) -> str:
        return self.__format_moment(clip, clip.description)

    def visit_highlight(self, highlight: HighlightEvent) -> str:
        return self.__format_moment(highlight, highlight.description)

    def visit_goal(self, goal: GoalEvent) -> str:
        match = self.__score_keeper.match
        scorer = goal.scorer
        if goal.is_away_goal():
            scorer = match.away_team.name

        return self.__format_moment(goal, f"⚽ But de {scorer} ({match.score.home}-{match.score.away})")

    def visit_outro(self, outro: OutroEvent) -> str:
        return ""

    def __format_moment(self, game_event: GameEvent, text: str) -> str:
        return f"{game_event.at.to_yt_format()} {text}\n"
