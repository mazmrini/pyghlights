from __future__ import annotations
from typing import List, Tuple

from moviepy.editor import VideoClip, ColorClip
from moviepy.video.VideoClip import ImageClip

from pyghlights.lib.clips.clip import Clip
from pyghlights.lib.clips.fxs import VideoFxs
from pyghlights.lib.events.events import (
    GameEvent,
    IntroEvent,
    ClipEvent,
    GoalEvent,
    OutroEvent,
    CompositeEvent,
    GameStartEvent,
    GameEndEvent,
    HighlightEvent,
)
from pyghlights.lib.events.visitors.clips.config.clip_config import ClipConfig
from pyghlights.lib.events.visitors.clips.clip_visitor import ClipVisitor
from pyghlights.lib.helpers.at import At
from pyghlights.lib.helpers.match import Match
from pyghlights.lib.logger import Logger


class SnippedEvent:
    def __init__(self, game_event: GameEvent, happens_at: At) -> None:
        self.game_event = game_event
        self.happens_at = happens_at


class SnipHighlightsVisitor(ClipVisitor):
    def __init__(self, clip: VideoClip, clip_config: ClipConfig, match: Match) -> None:
        self.__clip = clip
        self.__config = clip_config

    def visit(self, events: List[GameEvent]) -> Tuple[VideoClip, GameEvent]:
        out_clip = Clip(VideoFxs.from_str(self.__config.video_fx))
        out_events: List[GameEvent] = []
        current_at = At.zero()

        for event in [e for e in events if not e.skip_clip]:
            clip, _ = event.visit(self)
            out_clip.add(clip)
            out_events.append(event.with_at(current_at))

            current_at = current_at.plus_seconds(clip.duration)

        return out_clip.concatenate(), CompositeEvent.new(events=out_events)

    def visit_intro(self, intro: IntroEvent) -> Tuple[VideoClip, GameEvent]:
        if self.__config.intro.bg_path is not None:
            Logger.log(f"Using intro background @ {self.__config.intro.bg_path}")
            return ImageClip(str(self.__config.intro.bg_path), duration=self.__config.intro.duration), intro

        Logger.log("No intro background - default to intro background color")
        return (
            ColorClip(
                size=self.__clip.size, color=self.__config.intro.default_bg_color, duration=self.__config.intro.duration
            ),
            intro,
        )

    def visit_game_start(self, game_start: GameStartEvent) -> Tuple[VideoClip, GameEvent]:
        return (
            self.__clip.subclip(game_start.at.minus_seconds(2.25).seconds, game_start.at.plus_seconds(2.25).seconds),
            game_start,
        )

    def visit_game_end(self, game_end: GameEndEvent) -> Tuple[VideoClip, GameEvent]:
        return (
            self.__clip.subclip(game_end.at.minus_seconds(2.25).seconds, game_end.at.plus_seconds(2.25).seconds),
            game_end,
        )

    def visit_clip(self, clip_event: ClipEvent) -> Tuple[VideoClip, GameEvent]:
        return (
            self.__clip.subclip(
                clip_event.at.minus_seconds(clip_event.start_before).seconds,
                clip_event.at.plus_seconds(clip_event.end_after).seconds,
            ),
            clip_event,
        )

    def visit_highlight(self, highlight: HighlightEvent) -> Tuple[VideoClip, GameEvent]:
        buildup_range = self.__config.highlight.buildup.get_or_normal(highlight.buildup)
        return (
            self.__clip.subclip(
                highlight.at.minus_seconds(buildup_range.start_before).seconds,
                highlight.at.plus_seconds(buildup_range.end_after).seconds,
            ),
            highlight,
        )

    def visit_goal(self, goal: GoalEvent) -> Tuple[VideoClip, GameEvent]:
        buildup_range = self.__config.goal.buildup.get_or_normal(goal.buildup)
        return (
            self.__clip.subclip(
                goal.at.minus_seconds(buildup_range.start_before).seconds,
                goal.at.plus_seconds(buildup_range.end_after).seconds,
            ),
            goal,
        )

    def visit_outro(self, outro: OutroEvent) -> Tuple[VideoClip, GameEvent]:
        if self.__config.outro.bg_path is not None:
            Logger.log(f"Using outro background @ {self.__config.outro.bg_path}")
            return ImageClip(str(self.__config.outro.bg_path), duration=self.__config.outro.duration), outro

        Logger.log("No outro background - default to outro background color")
        return (
            ColorClip(
                size=self.__clip.size,
                color=self.__config.outro.default_bg_color,
                duration=self.__config.outro.duration,
            ),
            outro,
        )
