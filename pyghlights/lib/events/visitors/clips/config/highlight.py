from pydantic import BaseModel

from pyghlights.lib.events.visitors.clips.config.buildup import BuildupConfig, BuildupTimeRange


def _default_buildup() -> BuildupConfig:
    return BuildupConfig(
        very_fast=BuildupTimeRange.new(5, 4),
        fast=BuildupTimeRange.new(7, 4),
        normal=BuildupTimeRange.new(9.5, 4),
        slow=BuildupTimeRange.new(12, 4),
        very_slow=BuildupTimeRange.new(15, 4),
    )


class HighlightConfig(BaseModel):
    buildup: BuildupConfig = _default_buildup()
