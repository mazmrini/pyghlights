from typing import Tuple

from pydantic import BaseModel, ConfigDict, Extra, PositiveInt

from pyghlights.lib.events.visitors.clips.config.buildup import BuildupConfig, BuildupTimeRange
from pyghlights.lib.events.visitors.clips.config.font import FontConfig
from pyghlights.lib.helpers.pydantic import ColorInt


def _default_buildup() -> BuildupConfig:
    return BuildupConfig(
        very_fast=BuildupTimeRange.new(5, 4),
        fast=BuildupTimeRange.new(7, 4),
        normal=BuildupTimeRange.new(9.5, 4),
        slow=BuildupTimeRange.new(12, 4),
        very_slow=BuildupTimeRange.new(15, 4),
    )


class GoalConfig(BaseModel):
    model_config = ConfigDict(extra=Extra.forbid)

    banner_duration: PositiveInt = 4
    banner_bg_color: Tuple[ColorInt, ColorInt, ColorInt] = (33, 33, 33)
    font: FontConfig = FontConfig.text(color="snow2", size=48)
    buildup: BuildupConfig = _default_buildup()
