from __future__ import annotations

from typing import Literal, Union
from pydantic import Extra, BaseModel, ConfigDict, PositiveFloat


Buildup = Union[Literal["very_fast"], Literal["fast"], Literal["normal"], Literal["slow"], Literal["very_slow"]]


class BuildupTimeRange(BaseModel):
    model_config = ConfigDict(extra=Extra.forbid)

    start_before: PositiveFloat
    end_after: PositiveFloat

    @classmethod
    def new(cls, before: float, after: float) -> BuildupTimeRange:
        return BuildupTimeRange(start_before=before, end_after=after)


class BuildupConfig(BaseModel):
    model_config = ConfigDict(extra=Extra.forbid)

    very_fast: BuildupTimeRange
    fast: BuildupTimeRange
    normal: BuildupTimeRange
    slow: BuildupTimeRange
    very_slow: BuildupTimeRange

    def get_or_normal(self, value: str) -> BuildupTimeRange:
        return BuildupTimeRange.model_validate(self.model_dump().get(value, self.normal.model_dump()))
