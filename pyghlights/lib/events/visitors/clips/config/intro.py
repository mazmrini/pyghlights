from pydantic import ConfigDict, BaseModel, PositiveInt, Extra

from pyghlights.lib.events.visitors.clips.config.font import FontConfig
from pyghlights.lib.events.visitors.clips.config.types import RGB
from pyghlights.lib.helpers.pydantic import OptionalPath


class IntroConfig(BaseModel):
    model_config = ConfigDict(extra=Extra.forbid)

    duration: PositiveInt = 3
    bg_path: OptionalPath = None
    default_bg_color: RGB = (222, 222, 222)
    font: FontConfig = FontConfig.title()
