from __future__ import annotations
from pathlib import Path

import yaml
from pydantic import BaseModel, ConfigDict, Extra

from pyghlights.lib.clips.fxs import VideoFXValue
from pyghlights.lib.events.visitors.clips.config.goal import GoalConfig
from pyghlights.lib.events.visitors.clips.config.highlight import HighlightConfig
from pyghlights.lib.events.visitors.clips.config.intro import IntroConfig
from pyghlights.lib.events.visitors.clips.config.outro import OutroConfig
from pyghlights.lib.events.visitors.clips.config.score_overlay import ScoreOverlayConfig
from pyghlights.lib.logger import Logger


class ClipConfig(BaseModel):
    model_config = ConfigDict(extra=Extra.forbid)

    video_fx: VideoFXValue = "crossfade"

    score_overlay: ScoreOverlayConfig = ScoreOverlayConfig()
    intro: IntroConfig = IntroConfig()
    outro: OutroConfig = OutroConfig()

    goal: GoalConfig = GoalConfig()
    highlight: HighlightConfig = HighlightConfig()

    @classmethod
    def from_file(cls, path: Path) -> ClipConfig:
        if not path.exists():
            Logger.log("No configuration file - default to default configuration")
            return ClipConfig()

        Logger.log(f"Using configuration found @ {path}")
        data = yaml.safe_load(path.read_text())
        return ClipConfig.model_validate(data)
