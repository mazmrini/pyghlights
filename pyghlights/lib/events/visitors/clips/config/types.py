from typing import Annotated, Tuple

from moviepy.video.VideoClip import TextClip
from pydantic import BeforeValidator

from pyghlights.lib.helpers.pydantic import ColorInt


def _validate_font_exists(value: str) -> str:
    assert value in TextClip.list("font"), "font must exist"

    return value


def _validate_color_exists(value: str) -> str:
    assert bytes(value, "utf-8") in TextClip.list("color"), "color must exist"

    return value


FontFamily = Annotated[str, BeforeValidator(_validate_font_exists)]
FontColor = Annotated[str, BeforeValidator(_validate_color_exists)]
RGB = Tuple[ColorInt, ColorInt, ColorInt]
