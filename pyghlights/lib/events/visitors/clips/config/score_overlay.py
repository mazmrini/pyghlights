from typing import Annotated

from pydantic import ConfigDict, BaseModel, Extra

from pyghlights.lib.events.visitors.clips.config.font import FontConfig
from pyghlights.lib.events.visitors.clips.config.types import RGB
from pyghlights.lib.helpers.pos import Pos, PosAsStr
from pyghlights.lib.helpers.pydantic import OptionalPath


class ScoreOverlayConfig(BaseModel):
    model_config = ConfigDict(extra=Extra.forbid)

    bg_path: OptionalPath = None
    bg_home_score_pos: Annotated[Pos, PosAsStr] = Pos(x=50, y=75)
    bg_away_score_pos: Annotated[Pos, PosAsStr] = Pos(x=150, y=75)

    team_bg_color: RGB = (33, 33, 33)
    team_font: FontConfig = FontConfig.text(color="snow2")

    score_bg_color: RGB = (222, 222, 222)
    score_font: FontConfig = FontConfig.text(color="grey1")
