from __future__ import annotations

from pydantic import BaseModel, ConfigDict, Extra, PositiveInt

from pyghlights.lib.events.visitors.clips.config.types import FontFamily, FontColor


class FontConfig(BaseModel):
    model_config = ConfigDict(extra=Extra.forbid)

    family: FontFamily = "Arial"
    size: PositiveInt = 24
    color: FontColor = "black"

    @classmethod
    def title(cls, color: str = "black") -> FontConfig:
        return FontConfig(family="Arial", size=54, color=color)

    @classmethod
    def text(cls, color: str = "black", size: int = 36) -> FontConfig:
        return FontConfig(family="Segoe-UI-Symbol", size=size, color=color)
