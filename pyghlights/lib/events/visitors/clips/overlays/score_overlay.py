import abc

from moviepy.video.VideoClip import VideoClip

from pyghlights.lib.helpers.match import Score


class ScoreOverlay(abc.ABC):
    @abc.abstractmethod
    def make(self, score: Score) -> VideoClip:
        pass
