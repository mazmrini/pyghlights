from moviepy.editor import VideoClip, TextClip, ColorClip, CompositeVideoClip, clips_array

from pyghlights.lib.events.visitors.clips.config.score_overlay import ScoreOverlayConfig
from pyghlights.lib.events.visitors.clips.overlays.score_overlay import ScoreOverlay
from pyghlights.lib.helpers.match import Score
from pyghlights.lib.helpers.team_name import TeamName


class BasicScoreOverlay(ScoreOverlay):
    def __init__(self, config: ScoreOverlayConfig, home_team: TeamName, away_team: TeamName) -> None:
        self.__config = config
        self.__home_team = home_team
        self.__away_team = away_team

    def make(self, score: Score) -> VideoClip:
        home_text = self.__team_name_text_clip(self.__home_team)
        away_text = self.__team_name_text_clip(self.__away_team)
        name_size = (max(home_text.w, away_text.w), max(home_text.h, away_text.h))
        home_text.size = name_size
        away_text.size = name_size
        home_clip = self.__team_name_clip(home_text)
        away_clip = self.__team_name_clip(away_text)

        score_text = self.__score_text_clip(f"{score.home}-{score.away}")
        double_digit_score = self.__score_text_clip("12-34")
        score_bg = ColorClip(
            size=(int(double_digit_score.w * 1.1), int(home_clip.h * 1.5)),
            color=self.__config.score_bg_color,
        )
        score_clip = CompositeVideoClip([score_bg, score_text])

        return clips_array([[home_clip, score_clip, away_clip]])

    def __score_text_clip(self, score: str) -> TextClip:
        return TextClip(
            score,
            font=self.__config.score_font.family,
            fontsize=self.__config.score_font.size,
            color=self.__config.score_font.color,
            align="center",
        ).set_position("center")

    def __team_name_text_clip(self, name: TeamName) -> TextClip:
        return TextClip(
            name.short(),
            font=self.__config.team_font.family,
            fontsize=self.__config.team_font.size,
            color=self.__config.team_font.color,
            align="center",
        ).set_position("center")

    def __team_name_clip(self, text_clip: TextClip) -> VideoClip:
        bg_clip = ColorClip(
            size=(int(text_clip.w * 1.4), int(text_clip.h * 1.15)), color=self.__config.team_bg_color
        ).set_opacity(0.95)

        return CompositeVideoClip([bg_clip, text_clip])
