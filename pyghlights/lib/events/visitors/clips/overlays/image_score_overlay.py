from moviepy.editor import VideoClip, ImageClip, TextClip, CompositeVideoClip

from pyghlights.lib.events.visitors.clips.config.score_overlay import ScoreOverlayConfig
from pyghlights.lib.events.visitors.clips.overlays.score_overlay import ScoreOverlay
from pyghlights.lib.helpers.match import Score
from pyghlights.lib.helpers.pos import Pos


class ImageScoreOverlay(ScoreOverlay):
    def __init__(self, config: ScoreOverlayConfig) -> None:
        self.__config = config

    def make(self, score: Score) -> VideoClip:
        img_clip = ImageClip(str(self.__config.bg_path))
        home_score = self.__make_score_text(score.home, self.__config.bg_home_score_pos)
        away_score = self.__make_score_text(score.away, self.__config.bg_away_score_pos)

        return CompositeVideoClip([img_clip, home_score, away_score])

    def __make_score_text(self, value: int, pos: Pos) -> VideoClip:
        clip = TextClip(
            str(value),
            font=self.__config.score_font.family,
            fontsize=self.__config.score_font.size,
            color=self.__config.score_font.color,
        )
        x = int(pos.x - clip.w / 2)
        y = int(pos.y - clip.h / 2)

        return clip.set_position((x, y))
