from __future__ import annotations
from typing import List, Tuple

from moviepy.editor import VideoClip, TextClip, CompositeVideoClip, ColorClip, clips_array

from pyghlights.lib.clips.fxs import VideoFxs
from pyghlights.lib.events.events import (
    GameEvent,
    IntroEvent,
    ClipEvent,
    GoalEvent,
    OutroEvent,
    GameStartEvent,
    CompositeEvent,
    GameEndEvent,
    HighlightEvent,
)
from pyghlights.lib.events.visitors.clips.config.clip_config import ClipConfig
from pyghlights.lib.events.visitors.clips.clip_visitor import ClipVisitor
from pyghlights.lib.events.visitors.score_keeper_visitor import ScoreKeeperVisitor
from pyghlights.lib.helpers.match import Match


class AddClipOverlayVisitor(ClipVisitor):
    def __init__(self, clip: VideoClip, clip_config: ClipConfig, match: Match) -> None:
        self.__clip = clip
        self.__config = clip_config

        self.__score_keeper = ScoreKeeperVisitor(match)

    def visit(self, events: List[GameEvent]) -> Tuple[VideoClip, GameEvent]:
        self.__score_keeper.restart()
        overlays: List[VideoClip] = []

        for event in events:
            event.visit(self.__score_keeper)
            if event.skip_clip:
                continue

            overlay, _ = event.visit(self)
            overlays.append(VideoFxs.apply(self.__config.video_fx, overlay.set_start(event.at.seconds)))

        return (
            CompositeVideoClip([self.__clip, *overlays], use_bgclip=True).set_audio(self.__clip.audio),
            CompositeEvent.new(events=events),
        )

    def visit_intro(self, intro: IntroEvent) -> Tuple[VideoClip, GameEvent]:
        if self.__config.intro.bg_path:
            return self.__empty_clip().set_duration(self.__config.intro.duration), intro

        match = self.__score_keeper.match
        return (
            TextClip(
                f"{match.home_team.name}\nvs\n{match.away_team.name}",
                font=self.__config.intro.font.family,
                fontsize=self.__config.intro.font.size,
                color=self.__config.intro.font.color,
                align="center",
            )
            .set_duration(self.__config.intro.duration)
            .set_position("center")
        ), intro

    def visit_game_start(self, game_start: GameStartEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__empty_clip(), game_start

    def visit_game_end(self, game_end: GameEndEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__empty_clip(), game_end

    def visit_clip(self, clip_event: ClipEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__empty_clip(), clip_event

    def visit_highlight(self, highlight_event: HighlightEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__empty_clip(), highlight_event

    def visit_goal(self, goal: GoalEvent) -> Tuple[VideoClip, GameEvent]:
        scorer_text = "⚽ " * self.__score_keeper.match.nb_goals_for(goal.scorer) + f"   {goal.scorer}"
        if goal.is_away_goal():
            scorer_text = f"⚽   {self.__score_keeper.match.away_team.name}"

        subtext = (
            TextClip(
                scorer_text,
                font=self.__config.goal.font.family,
                fontsize=self.__config.goal.font.size,
                color=self.__config.goal.font.color,
                align="West",
            )
            .set_opacity(0.9)
            .margin(top=self.__config.goal.font.size // 4, bottom=self.__config.goal.font.size // 4, opacity=0)
        )
        subtext = subtext.margin(
            left=self.__config.goal.font.size, right=int(max(self.__clip.w * 0.4, subtext.w)), opacity=0
        )
        subtext_bg = ColorClip(
            size=subtext.size, color=self.__config.goal.banner_bg_color, duration=subtext.duration
        ).set_opacity(0.75)

        return (
            CompositeVideoClip([subtext_bg, subtext])
            .set_position((0, 0.8), relative=True)
            .set_duration(self.__config.goal.banner_duration)
        ), goal

    def visit_outro(self, outro: OutroEvent) -> Tuple[VideoClip, GameEvent]:
        if self.__config.outro.bg_path:
            return self.__empty_clip().set_duration(self.__config.outro.duration), outro

        match = self.__score_keeper.match
        top_scorers_stat = match.top_scorers()

        game_score = TextClip(
            f"{match.home_team.short()}  {match.score.home}-{match.score.away}  {match.away_team.short()}",
            font=self.__config.outro.title_font.family,
            fontsize=self.__config.outro.title_font.size,
            color=self.__config.outro.title_font.color,
            align="center",
        ).margin(bottom=self.__config.outro.title_font.size // 2, opacity=0)
        top_scorers_names = TextClip(
            "\n".join(player.name for player in top_scorers_stat),
            font=self.__config.outro.text_font.family,
            fontsize=self.__config.outro.text_font.size,
            color=self.__config.outro.text_font.color,
            align="East",
        ).margin(self.__config.outro.text_font.size // 2, opacity=0)
        top_scorers_goals = TextClip(
            "\n".join("⚽ " * player.goals for player in top_scorers_stat),
            font=self.__config.outro.text_font.family,
            fontsize=self.__config.outro.text_font.size,
            color=self.__config.outro.text_font.color,
            align="West",
        ).margin(self.__config.outro.text_font.size // 2, opacity=0)

        return (
            clips_array([[game_score], [clips_array([[top_scorers_names, top_scorers_goals]])]])
            .set_duration(self.__config.outro.duration)
            .set_position("center"),
            outro,
        )

    def __empty_clip(self) -> VideoClip:
        return TextClip(" ").set_opacity(0).set_duration(0.1)
