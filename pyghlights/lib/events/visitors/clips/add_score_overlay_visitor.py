from __future__ import annotations
from typing import List, Tuple

from moviepy.editor import VideoClip, TextClip, CompositeVideoClip

from pyghlights.lib.events.events import (
    GameEvent,
    IntroEvent,
    ClipEvent,
    GoalEvent,
    OutroEvent,
    GameStartEvent,
    GameEndEvent,
    CompositeEvent,
    HighlightEvent,
)
from pyghlights.lib.events.visitors.clips.config.clip_config import ClipConfig
from pyghlights.lib.events.visitors.clips.clip_visitor import ClipVisitor
from pyghlights.lib.events.visitors.clips.overlays.basic_score_overlay import BasicScoreOverlay
from pyghlights.lib.events.visitors.clips.overlays.image_score_overlay import ImageScoreOverlay
from pyghlights.lib.events.visitors.clips.overlays.score_overlay import ScoreOverlay
from pyghlights.lib.events.visitors.score_keeper_visitor import ScoreKeeperVisitor
from pyghlights.lib.helpers.match import Match
from pyghlights.lib.logger import Logger


class AddScoreOverlayVisitor(ClipVisitor):
    def __init__(self, clip: VideoClip, clip_config: ClipConfig, match: Match) -> None:
        self.__clip = clip
        self.__config = clip_config

        self.__score_keeper = ScoreKeeperVisitor(match)
        if clip_config.score_overlay.bg_path is None:
            Logger.log("No score overlay background - default to the basic one")
        else:
            Logger.log(f"Using score overlay background @ {clip_config.score_overlay.bg_path}")

    def visit(self, events: List[GameEvent]) -> Tuple[VideoClip, GameEvent]:
        self.__score_keeper.restart()
        overlays: List[VideoClip] = []
        for i, event in enumerate(events[:-1]):
            event.visit(self.__score_keeper)
            if event.skip_clip:
                continue

            next_event = events[i + 1]
            overlay, _ = event.visit(self)
            overlays.append(overlay.set_start(event.at.seconds).set_end(next_event.at.seconds))

        overlays[-1] = overlays[-1].set_end(self.__clip.end)

        return (
            CompositeVideoClip([self.__clip, *overlays], use_bgclip=True).set_audio(self.__clip.audio),
            CompositeEvent.new(events=events),
        )

    def visit_intro(self, intro: IntroEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__empty_clip(), intro

    def visit_game_start(self, game_start: GameStartEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__make_score_overlay(), game_start

    def visit_game_end(self, game_end: GameEndEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__make_score_overlay(True), game_end

    def visit_clip(self, clip_event: ClipEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__make_score_overlay(), clip_event

    def visit_highlight(self, highlight_event: HighlightEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__make_score_overlay(), highlight_event

    def visit_goal(self, goal: GoalEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__make_score_overlay(), goal

    def visit_outro(self, outro: OutroEvent) -> Tuple[VideoClip, GameEvent]:
        return self.__empty_clip(), outro

    def __make_score_overlay(self, force: bool = False) -> VideoClip:
        if not force and (not self.__score_keeper.is_game_started or self.__score_keeper.is_game_ended):
            return self.__empty_clip()

        match = self.__score_keeper.match
        overlay: ScoreOverlay = BasicScoreOverlay(self.__config.score_overlay, match.home_team, match.away_team)
        if self.__config.score_overlay.bg_path is not None:
            overlay = ImageScoreOverlay(self.__config.score_overlay)

        return overlay.make(match.score)

    def __empty_clip(self) -> VideoClip:
        return TextClip(" ").set_opacity(0)
