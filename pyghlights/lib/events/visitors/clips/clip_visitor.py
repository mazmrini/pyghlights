from typing import Tuple, Callable

from pyghlights.lib.events.events import GameEventVisitor, GameEvent
from moviepy.editor import VideoClip

from pyghlights.lib.events.visitors.clips.config.clip_config import ClipConfig
from pyghlights.lib.helpers.match import Match

ClipVisitor = GameEventVisitor[Tuple[VideoClip, GameEvent]]
ClipVisitorFactory = Callable[[VideoClip, ClipConfig, Match], ClipVisitor]
