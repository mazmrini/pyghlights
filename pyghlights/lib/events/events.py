from __future__ import annotations
import abc
from typing import TypeVar, Generic, List, Union, Literal, Annotated, Dict, Any

from pydantic import BaseModel, ConfigDict, Extra, NonNegativeFloat

from pyghlights.lib.events.visitors.clips.config.buildup import Buildup
from pyghlights.lib.helpers.at import At, AtAsDto
from pyghlights.lib.helpers.const import AWAY

T = TypeVar("T")


class GameEvent(abc.ABC, BaseModel):
    model_config = ConfigDict(extra=Extra.forbid, str_min_length=1)

    at: Annotated[At, AtAsDto]
    skip_clip: bool = False

    @abc.abstractmethod
    def visit(self, visitor: GameEventVisitor[T]) -> T:
        pass

    @abc.abstractmethod
    def with_at(self, at: At) -> GameEvent:
        pass

    @staticmethod
    def examples() -> List[Dict[str, Any]]:
        return [
            {"type": "game_start", "at": 10},
            {"type": "clip", "at": 20, "description": "desc", "start_before": 3, "end_after": 5},
            {"type": "highlight", "at": 30, "description": "desc", "buildup": "slow"},
            {"type": "goal", "at": 40, "scorer": "Smith", "buildup": "fast"},
            {"type": "game_end", "at": 50},
        ]


class GameEventVisitor(abc.ABC, Generic[T]):
    @abc.abstractmethod
    def visit(self, events: List[GameEvent]) -> T:
        pass

    @abc.abstractmethod
    def visit_intro(self, intro: IntroEvent) -> T:
        pass

    @abc.abstractmethod
    def visit_game_start(self, game_start: GameStartEvent) -> T:
        pass

    @abc.abstractmethod
    def visit_game_end(self, game_end: GameEndEvent) -> T:
        pass

    @abc.abstractmethod
    def visit_clip(self, clip: ClipEvent) -> T:
        pass

    @abc.abstractmethod
    def visit_highlight(self, highlight: HighlightEvent) -> T:
        pass

    @abc.abstractmethod
    def visit_goal(self, goal: GoalEvent) -> T:
        pass

    @abc.abstractmethod
    def visit_outro(self, outro: OutroEvent) -> T:
        pass


class CompositeEvent(GameEvent):
    events: List[GameEvent]

    @classmethod
    def new(cls, events: List[GameEvent]) -> CompositeEvent:
        return CompositeEvent(at=At.zero(), events=events)

    def visit(self, visitor: GameEventVisitor[T]) -> T:
        return visitor.visit(self.events)

    def with_at(self, at: At) -> GameEvent:
        return self


class IntroEvent(GameEvent):
    @classmethod
    def new(cls) -> IntroEvent:
        return IntroEvent(at=At.zero())

    def visit(self, visitor: GameEventVisitor[T]) -> T:
        return visitor.visit_intro(self)

    def with_at(self, at: At) -> GameEvent:
        return IntroEvent(at=at, skip_clip=self.skip_clip)


class ClipEvent(GameEvent):
    description: str
    start_before: NonNegativeFloat = 0
    end_after: NonNegativeFloat
    type: Literal["clip"] = "clip"

    def visit(self, visitor: GameEventVisitor[T]) -> T:
        return visitor.visit_clip(self)

    def with_at(self, at: At) -> GameEvent:
        return ClipEvent(
            at=at,
            description=self.description,
            start_before=self.start_before,
            end_after=self.end_after,
            skip_clip=self.skip_clip,
        )


class GameStartEvent(GameEvent):
    type: Literal["game_start"] = "game_start"

    def visit(self, visitor: GameEventVisitor[T]) -> T:
        return visitor.visit_game_start(self)

    def with_at(self, at: At) -> GameEvent:
        return GameStartEvent(at=at, skip_clip=self.skip_clip)


class GameEndEvent(GameEvent):
    type: Literal["game_end"] = "game_end"

    def visit(self, visitor: GameEventVisitor[T]) -> T:
        return visitor.visit_game_end(self)

    def with_at(self, at: At) -> GameEvent:
        return GameEndEvent(at=at, skip_clip=self.skip_clip)


class HighlightEvent(GameEvent):
    description: str
    buildup: Buildup = "normal"
    type: Literal["highlight"] = "highlight"

    def visit(self, visitor: GameEventVisitor[T]) -> T:
        return visitor.visit_highlight(self)

    def with_at(self, at: At) -> GameEvent:
        return HighlightEvent(at=at, description=self.description, buildup=self.buildup, skip_clip=self.skip_clip)


class GoalEvent(GameEvent):
    scorer: str
    buildup: Buildup = "normal"
    type: Literal["goal"] = "goal"

    @classmethod
    def away(cls, at: At, buildup: Buildup) -> GoalEvent:
        return GoalEvent(at=at, scorer=AWAY, buildup=buildup)

    def visit(self, visitor: GameEventVisitor[T]) -> T:
        return visitor.visit_goal(self)

    def with_at(self, at: At) -> GameEvent:
        return GoalEvent(at=at, scorer=self.scorer, buildup=self.buildup, skip_clip=self.skip_clip)

    def is_away_goal(self) -> bool:
        return self.scorer == AWAY


class OutroEvent(GameEvent):
    @classmethod
    def new(cls) -> OutroEvent:
        return OutroEvent(at=At.infinity())

    def visit(self, visitor: GameEventVisitor[T]) -> T:
        return visitor.visit_outro(self)

    def with_at(self, at: At) -> GameEvent:
        return OutroEvent(at=at, skip_clip=self.skip_clip)


SerializableGameEvent = Union[GameStartEvent, GameEndEvent, ClipEvent, HighlightEvent, GoalEvent]
